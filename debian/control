Source: python-urwidtrees
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: ChangZhuo Chen (陳昌倬) <czchen@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-urwid (>= 2.1.0-4),
               python3-setuptools
Standards-Version: 4.6.2
Homepage: https://github.com/pazz/urwidtrees
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-urwidtrees
Vcs-Git: https://salsa.debian.org/python-team/packages/python-urwidtrees.git
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-python

Package: python3-urwidtrees
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${python3:Depends},
Description: Urwid Tree Container API
 This is a Widget Container API for the urwid toolkit. It uses a MVC
 approach and allows one to build trees of widgets. Its design goals are:
 .
  * clear separation classes that define, decorate and display trees of
    widgets
  * representation of trees by local operations on node positions
  * easy to use default implementation for simple trees
  * Collapses are considered decoration.
 .
 This package is for Python 3 environment.
